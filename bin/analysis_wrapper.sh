analysis_usage() {
    echo 
    echo -e "To analyse a run use '\x1b[1;49;33manalysis {run number} --{arguments}\x1b[0m'"
    echo
    echo -e "\x1b[1;49;35marguments for analysis\x1b[0m (if not specified the default version in config.yml is used):"
    echo
    echo -e "\x1b[1;49;35mpositional argument <int> \x1b[0m 'run number to analyze'"
    echo
    echo -e "\x1b[0;49;35m--fast_analysis <bool>\x1b[0m 'If True only basic HV point plots and the efficiency for max trigger window will be produced.'"
    echo -e "\x1b[0;49;35m--config <path>\x1b[0m 'overwrite the default config file (config.yml) to another .yml file'"
    echo -e "\x1b[0;49;35m--version <int>\x1b[0m 'iRPC version, options are 3 or 4 for RE31 or RE41 iRPC respectively.'"
    echo -e "\x1b[0;49;35m--position <str>\x1b[0m 'Position of iRPC detector where data are taken, options are LEFT or RIGHT.'"
    echo -e "\x1b[0;49;35m--single_hvpoint <bool>\x1b[0m 'If True only the plots for the single HV point will be produced'"
    echo -e "\x1b[0;49;35m--triggers <int>\x1b[0m 'Number of triggers taken per high voltage point."
    echo -e "\x1b[0;49;35m--dac <int>\x1b[0m 'dac value used to take data with (used for styling only), options are 7, 10, or 15.'"
    echo -e "\x1b[0;49;35m--dac_threshold <float>\x1b[0m 'dac threshold value used to take data with (used for styling only), must correspond to the dac setting.'"
    echo -e "\x1b[0;49;35m--source <str>\x1b[0m 'source of GIF++ when data was taken (used for styling only).'"z
    echo
}




analysis() {
    local this_dir="$( cd "$( dirname "${this_file}" )" && pwd )"
    for arg in "$@"; do
        if [ "$arg" == "--help" ]; then
            analysis_usage
            return 0
        fi
    done

    python ${this_dir}/IrpcAnalysisFramework/analysis.py "$@"

}



analysis_init() {
    local this_dir="$( cd "$( dirname "${this_file}" )" && pwd )"

    echo -e "\x1b[0;49;35m"
    printf '%*s\n' "${COLUMNS:-$(tput cols)}" '' | tr ' ' =

    echo -e "\x1b[1;49;33m"
    cat "${this_dir}/bin/ascii.txt"
    
    echo -e "\x1b[0m"
    analysis_usage
    echo
}
    
