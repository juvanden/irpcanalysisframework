#!/usr/bin/env bash

create_venv() {
    # Creates a new virtual environment inside $VENV_BASE and makes it relocatable.
    #
    # Arguments:
    #   1. name
    #       The name of the virtual env inside $VENV_BASE.
    #
    # Required environment variables:
    #   VENV_BASE:
    #       The base path where virtual environments are stored.

    # zsh options
    local shell_is_zsh="$( [ -z "${ZSH_VERSION}" ] && echo "false" || echo "true" )"
    if ${shell_is_zsh}; then
        emulate -L bash
        setopt globdots
    fi

    # check arguments
    local name="$1"
    if [ -z "${name}" ]; then
        >&2 echo "argument 0 (venv name) must not be empty"
        return "1"
    fi

    # check environment variables
    if [ -z "${VENV_BASE}" ]; then
        >&2 echo "environment variable VENV_BASE must not be empty"
        return "2"
    fi

    # create the venv the usual way, use symlinks with those pointing outside the venv being
    # transformed into copies when making it relocatable
    python3 -m venv --symlinks --upgrade-deps "${VENV_BASE}/${name}" || return "$?"
}