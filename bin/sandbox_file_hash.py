#!/usr/bin/env python
# coding: utf-8

"""
Standalone script that takes the path to a sandbox file and returns a unique hash based on its path
relative to certain environment variables.

the path will be relative to $CF_BASE.

"""

import os
import hashlib


def create_sandbox_file_hash(
    sandbox_file: str,
    return_path: bool = False,
) -> str:
    """
    If *return_path* is *True*, this function does not return the hash but rather the path
    determined by the mechanism above.

    The hash itself is based on the first eight characters of the sha1sum of the relative sandbox
    file.
    """
    # prepare paths
    abs_sandbox_file = real_path(sandbox_file)
    base = real_path(os.environ["BASE"])

    # determine the base paths
    rel_to_base = is_relative_to(abs_sandbox_file, base)

    # relative to BASE
    path_to_hash = os.path.join("$BASE", os.path.relpath(abs_sandbox_file, base))

    if return_path:
        return path_to_hash

    return hashlib.sha1(path_to_hash.encode("utf-8")).hexdigest()[:8]


def real_path(*path: str) -> str:
    """
    Takes *path* fragments and returns the real, absolute location with all variables expanded.
    """
    path = os.path.join(*map(str, path))
    while "$" in path or "~" in path:
        path = os.path.expandvars(os.path.expanduser(path))

    return os.path.realpath(path)


def is_relative_to(path: str, base: str) -> bool:
    """
    Returns *True* if a *path* is contained at any depth inside a *base* path, and *False*
    otherwise.
    """
    return not os.path.relpath(real_path(path), real_path(base)).startswith("..")


if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser(
        description="creates a hash of a sandbox file",
    )
    parser.add_argument(
        "sandbox_file",
        help="the sandbox file",
    )
    parser.add_argument(
        "--path",
        "-p",
        action="store_true",
        help="print the path determined for hashing rather than the hash itself",
    )
    args = parser.parse_args()

    print(create_sandbox_file_hash(args.sandbox_file, return_path=args.path))
