# IRPC-QC3-Daq-Ghent

This repository contains the framework to convert Irpc data collected by the FEB to a ttree format (.root) and plot results.

## Getting started:

1. Clone this repository on your local computer (or lxplus) and go inside this repository:
```
git clone https://gitlab.cern.ch/juvanden/irpcanalysisframework.git
cd irpcanalysisframework
```

2. run the setup of the analysis framework which will create and activate a virtual environment
```
source setup.sh
```

## Run an analysis:

1. check if the configuration file in `IrpcAnalysisFramework/config.yml` contains the correct settings for the Irpc detector and scan you want to analyze. 

2. run the command `analysis {run number}` to start an analysis.

## Customization:

Settings for analyzing the iRPC data by two different methods.

1. Modifying the configuration file (default `IrpcAnalysisFramework/config.yml`). The structure of this configuration file is as follows:
    - detector (detector settings)
        - version (can be either 3 or 4 for RE31 and RE41 iRPC detector respectively)
        - position (LEFT or RIGHT position of the FEB where the iRPC took data)
        - links (number of links used for data taking)
        - chamber1 (chamber number of link 1)
        - chamber2 (chamber number of link 2)
    - scan (scan speficic settings)
        - triggers (number of triggers per high voltage point of the scan)
        - HVpoints (number of high voltage points in the scan)
        - HVvalues (values of the high voltage points, needs to be a list with length of HVpoints)
    - paths (path directories required for analysis)
        - offset_file (file that provides the offset for each strip of the iRPC detector)
        - profile_file (file that provides the length of each strip of the iRPC detector)
        - output (directory where ttree output and results are saved)
        - data (directory where data is transfered to from the remote desktop)
    - styling (settings that are only used for clarity & plotting purposes)
        - cmslabel (label on top right of result plots)
        - dac (dac number used for the data taking, can be 7, 10 or 15)
        - dac_threshold (charge threshold that corresponds to the dac number)
        - source (source value off the GIF++ radiation source if applicable)

2. Add an argument flag to `analysis {run number}` when running an analysis. The possible argument flags that can be given are:
    - `--fast_analysis [True/False]`: If True the analysis only produces the efficiency result of a single trigger window without plotting all high voltage point results.
    - `--config [path]`: If given, it overwrites the default config file
    - `--dac [int]`
    - `--dac_threshold [float]`
    - `--source [str]`




