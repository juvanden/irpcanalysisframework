#!/usr/bin/env bash

#
# global configurable variables
#

# desktop to get data from (ex.: "hkou@libo-pc-42.dyndns.cern.ch:")
# TODO write the password of the desktop to not have a password query
export SSH_DIRECTORY="hkou@libo-pc-42.dyndns.cern.ch:"
export SSH_PASSWORD=""

# directory in desktop where data is stored
export DATA_DIRECTORY="/home/hkou/iRPC2E/DAQ_10G/" 
