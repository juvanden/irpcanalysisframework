#!/usr/bin/env bash


parse() {
    

    while [[ "$#" -gt 0 ]]; do
        case $1 in
            --config)
                local CONFIG_FILE="$2"
                shift 2
                ;;
            --help)
                usage
                ;;
        esac
    done
}

setup() {
    local shell_is_zsh="$( [ -z "${ZSH_VERSION}" ] && echo "false" || echo "true" )"
    local this_file="$( ${shell_is_zsh} && echo "${(%):-%x}" || echo "${BASH_SOURCE[0]}" )"
    local this_dir="$( cd "$( dirname "${this_file}" )" && pwd )"
    local orig="${PWD}"
    local setup_name="${1:-default}"
    local setup_is_default="false"
    local pyv="3.10.12"
    [ "${setup_name}" = "default" ] && setup_is_default="true"


    # zsh options
    if ${shell_is_zsh}; then
        emulate -L bash
        setopt globdots
    fi


    #
    # global variables
    #

    # start exporting variables and create venv
    export BASE="${this_dir}"
    export VENV_BASE="${BASE}/data/software/venv"
    export CONDA_BASE="${BASE}/data/software/conda"
    export ANALYSIS_BASE="${BASE}/IrpcAnalysisFramework"
    
    # update paths and flags
    export MAMBA_ROOT_PREFIX="${CONDA_BASE}"
    export MAMBA_EXE="${MAMBA_ROOT_PREFIX}/bin/micromamba"
    export PYTHONWARNINGS="${PYTHONWARNINGS:-ignore}"

    
    local conda_missing="$( [ -d "${CONDA_BASE}" ] && echo "false" || echo "true" )"
    if ${conda_missing}; then
        echo
        bash_color magenta "installing conda with micromamba interface at ${CONDA_BASE}"

        mkdir -p "${CONDA_BASE}/etc/profile.d"
        curl -L https://micro.mamba.pm/api/micromamba/linux-64/latest | tar -xvj -C "${CONDA_BASE}" "bin/micromamba" > /dev/null
        2>&1 "${CONDA_BASE}/bin/micromamba" shell hook -y --prefix="$PWD" &> micromamba.sh || return "$?"
        
        # make the setup file relocatable
        sed -i -r "s|${CONDA_BASE}|\$\{MAMBA_ROOT_PREFIX\}|" "micromamba.sh" || return "$?"
        mv "micromamba.sh" "${CONDA_BASE}/etc/profile.d/micromamba.sh"
        cat << EOF > "${CONDA_BASE}/.mambarc"
changeps1: false
always_yes: true
channels:
  - conda-forge
EOF
    fi

    # initialize micromamba
    source "${CONDA_BASE}/etc/profile.d/micromamba.sh" "" || return "$?"
    micromamba activate || return "$?"
    echo "initialized conda with $( bash_color magenta "micromamba" ) interface and $( bash_color magenta "python ${pyv}" )"

    # install packages
    if ${conda_missing}; then
        echo
        bash_color cyan "setting up conda / micromamba environment"
        micromamba install \
            libgcc \
            bash \
            zsh \
            "python=${pyv}" \
            git \
            git-lfs \
            myproxy \
            conda-pack \
            || return "$?"
        micromamba clean --yes --all
    fi

    source "${BASE}/bin/create_venv.sh"
    source "${BASE}/bin/sandbox_file_hash"
    source "${BASE}/sandboxes/venv.sh"

    # source the config bash file
    parse "$@"
    if ! [ -n "${CONFIG_FILE}" ]; then
        local CONFIG_FILE="cfg.sh"
    fi

    source "${CONFIG_FILE}"
}   


create_venv() {
    # Creates a new virtual environment inside $VENV_BASE and makes it relocatable.
    #
    # Arguments:
    #   1. name
    #       The name of the virtual env inside $VENV_BASE.
    #
    # Required environment variables:
    #   VENV_BASE:
    #       The base path where virtual environments are stored.

    # zsh options
    local shell_is_zsh="$( [ -z "${ZSH_VERSION}" ] && echo "false" || echo "true" )"
    if ${shell_is_zsh}; then
        emulate -L bash
        setopt globdots
    fi

    # check arguments
    local name="$1"
    if [ -z "${name}" ]; then
        >&2 echo "argument 0 (venv name) must not be empty"
        return "1"
    fi

    # check environment variables
    if [ -z "${VENV_BASE}" ]; then
        >&2 echo "environment variable VENV_BASE must not be empty"
        return "2"
    fi

    # create the venv the usual way, use symlinks with those pointing outside the venv being
    # transformed into copies when making it relocatable
    python3 -m venv --symlinks --upgrade-deps "${VENV_BASE}/${name}" || return "$?"

}


bash_color() {
    # get arguments
    local color="$1"
    local msg="${@:2}"

    # zsh options
    local shell_is_zsh="$( [ -z "${ZSH_VERSION}" ] && echo "false" || echo "true" )"
    if ${shell_is_zsh}; then
        emulate -L bash
        setopt globdots
    fi

    case "${color}" in
        default)
            echo -e "\x1b[0;49;39m${msg}\x1b[0m"
            ;;
        red)
            echo -e "\x1b[0;49;31m${msg}\x1b[0m"
            ;;
        green)
            echo -e "\x1b[0;49;32m${msg}\x1b[0m"
            ;;
        yellow)
            echo -e "\x1b[0;49;33m${msg}\x1b[0m"
            ;;
        blue)
            echo -e "\x1b[0;49;34m${msg}\x1b[0m"
            ;;
        magenta)
            echo -e "\x1b[0;49;35m${msg}\x1b[0m"
            ;;
        cyan)
            echo -e "\x1b[0;49;36m${msg}\x1b[0m"
            ;;
        default_bright)
            echo -e "\x1b[1;49;39m${msg}\x1b[0m"
            ;;
        red_bright)
            echo -e "\x1b[1;49;31m${msg}\x1b[0m"
            ;;
        green_bright)
            echo -e "\x1b[1;49;32m${msg}\x1b[0m"
            ;;
        yellow_bright)
            echo -e "\x1b[1;49;33m${msg}\x1b[0m"
            ;;
        blue_bright)
            echo -e "\x1b[1;49;34m${msg}\x1b[0m"
            ;;
        magenta_bright)
            echo -e "\x1b[1;49;35m${msg}\x1b[0m"
            ;;
        cyan_bright)
            echo -e "\x1b[1;49;36m${msg}\x1b[0m"
            ;;
        *)
            echo "${msg}"
            ;;
    esac
}
[ ! -z "${BASH_VERSION}" ] && export -f bash_color

main() {
    # Invokes the main action of this script, catches possible error codes and prints a message.

    # run the actual setup
    if setup "$@"; then
        bash_color green "software successfully setup"
        bash_color green "RPC data will be downloaded from ${SSH_DIRECTORY}${DATA_DIRECTORY}/"

        # instructions
        source "${BASE}/bin/analysis_wrapper.sh"
        analysis_init

        return "0"
    else
        local code="$?"
        bash_color red "software setup failed with code ${code}"
        return "${code}"
    fi
}


# entry point
if [ "${SKIP_SETUP}" != "1" ]; then
    main "$@"
fi