# coding: utf-8

"""
Main analysis script that calls subscripts 
"""

import os
from dotmap import DotMap
from util.read_config import read_config, print_config
from flatten_dict import flatten
from flatten_dict import unflatten

from conversion.conversion import main_conversion
from efficiency.efficiency import main_efficiency


def transfer_data_from_remote(
    config: DotMap,
):
    width = os.get_terminal_size().columns
    print("\n\x1b[1;49;33m", '-' * (width // 4),
          f"transfering data from {os.environ['SSH_DIRECTORY']}", '-' * (width // 4), "\x1b[0m\n")
    input_path = config.paths.remote
    output_path = config.paths.data

    # assume that if the directory exists and is not empty: all files are correctly transferred
    # optimal would be to check input and output paths for all files but less straightforward
    # TODO add password saving
    if os.path.isdir(output_path):
        if os.listdir(output_path):
            print("data files are already transfered")
            return
    else:
        os.makedirs(output_path)

    try:
        os.system(f"scp -r {input_path} {output_path}.")
    except:
        # if copying files failed remove output_path so os.path.isdir(output_path) would fail
        os.system(f"rm -r {output_path}")
        print(f"Copying data files of run{config.run} from {os.environ['SSH_DIRECTORY']} failed.\
            Check if the remote desktop is accessible and the requested data is available.")
        quit()


def update_config(args, config):

    default_config = read_config(config)
    config = {
        "run": args.run,
        "fast_analysis": args.fast_analysis,
        "single_hvpoint": args.single_hvpoint,
        "detector": {
            "position": args.position if args.position else default_config.detector.position,
        },
        "styling": {
            "dac": args.dac if args.dac else default_config.styling.dac,
            "dac_threshold": args.dac_threshold if args.dac_threshold else default_config.styling.dac_threshold,
            "source": args.source if args.source else default_config.styling.source,
        },
        "paths": {
            "data": default_config.paths.data + f"run{args.run}/",
            "output": default_config.paths.output + f"run{args.run}/",
            "remote": os.environ["SSH_DIRECTORY"] + os.environ["DATA_DIRECTORY"] + f"run{args.run}*",
        },
        "scan": {
            "triggers": args.triggers if args.triggers else default_config.scan.triggers,
            "HVpoints": 1 if args.single_hvpoint else default_config.scan.HVpoints,
            "HVvalues": [] if args.single_hvpoint else default_config.scan.HVvalues,
        }
    }

    # overwrite default_config with config
    config = unflatten({**flatten(default_config.toDict()), **flatten(config)})
    return DotMap(config)


# main
if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser(
        prog='iRPC-Analysis',
        description='Main script to analyse the data collected by the iRPC detector',
                    epilog='')

    parser.add_argument('run', help="run number to analyse (required)", type=int)
    parser.add_argument('--position', choices=["LEFT", "RIGHT"], type=str, help='position of iRPC detector where data are taken, options are LEFT or RIGHT.\
        If not specified the default version in config.yml is used')
    parser.add_argument('--dac', choices=[7, 10, 15], type=int, help='dac value used to take data with (used for styling only), options are 7, 10, or 15.\
        If not specified the default version in config.yml is used')
    parser.add_argument('--dac_threshold', type=int, help='dac threshold value used to take data with (used for styling only), must correspond to the dac setting.\
        If not specified the default version in config.yml is used')
    parser.add_argument('--source', type=str, help='source of GIF++ when data was taken (used for styling only).\
        If not specified the default version in config.yml is used')
    parser.add_argument('--triggers', type=int, help='number of triggers taken per high voltage point.\
        If not specified the default version in config.yml is used')
    parser.add_argument('--fast_analysis', action='store_true',
                        help='If True only basic HV point plots and the efficiency for max trigger window will be produced.')
    parser.add_argument('--single_hvpoint', action='store_true',
                        help='If True only the plots for the single HV point will be produced.')
    parser.add_argument('--config', type=str, default="config.yml",
                        help='overwrite the default config file (config.yml) to another .yml file')

    args = parser.parse_args()

    config = update_config(args, args.config)
    print_config(config)
    transfer_data_from_remote(config)

    errors = main_conversion(config)

    if (not errors) & (not config.single_hvpoint):
        main_efficiency(config)
