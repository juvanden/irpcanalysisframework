#!/usr/bin/python
# coding: utf-8

"""
Script to convert raw binary data to a root TTree
"""

import os
from dotmap import DotMap
from re import findall
import numpy as np
import awkward as ak
import pandas as pd
import uproot
import sys
from datetime import datetime

import conversion.BEE_analysis_multi_link_gif_v3 as BEE_analysis_multi_link_gif_v3
from conversion.BEE_analysis_multi_link_gif_v3 import Event
from conversion.BEE_analysis_multi_link_gif_v3 import channel2position
from conversion.BEE_analysis_multi_link_gif_v3 import Hit_info

from util.strip_mapping import get_strip_position

from plotting.plot import fast_plots, main_plots


def find_data(
    path: str,
    run: int,
    hv: int,
):
    """
    FIND PATH OF DATA FILE AT SPECIFIC HIGH VOLTAGE POINT
    :param path: absolute path of data folder
    :param HV_value: High voltage point
    :return: FULL PATH
    """
    match_flag = 0
    dir_list = os.listdir(path)
    for item in dir_list:
        match_result = findall(f"run{run}_.*_HV{hv}_.*", item)
        if (match_result != []):
            match_flag = 1
            break
    if (match_flag == 1):
        return path + match_result[0]
    else:
        print(f"The data for run {run} at hv{hv} was not found in {path}")
        return None


def progress(count, total, suffix=''):
    bar_len = 60
    filled_len = int(round(bar_len * count / float(total)))

    percents = round(100.0 * count / float(total), 1)
    bar = '=' * filled_len + '-' * (bar_len - filled_len)

    sys.stdout.write('\r[%s] %s%s ...%s\r' % (bar, percents, '%', suffix))
    sys.stdout.flush()
    if count == total:
        print("\x1b[0;49;32m", '\r[%s] %s%s ...%s\r' % (bar, percents, '%', suffix))


def converted(
    config: DotMap,
    path: str,
) -> bool:

    if not os.path.isfile(path):
        return False

    if not path.endswith(".root"):
        return False

    with uproot.open(path) as file:

        if (len(file.keys()) != 5) & (not config.fast_analysis):
            return False

        # prevent from skipping convertion if previous failed and ttree was never written out
        elif (len(file.keys()) == 0) & (config.fast_analysis):
            return False
        # prevent from skipping convertion if the events do not match triggers
        # TODO change this to give a warning and continue instead of doing the convertion again
        if len(np.array(file["events"]["L1T"])) != config.scan.triggers:
            return False

    return True


def convert(
    config: DotMap,
    input_path: str,
    output_dir: str,
    hv: int
) -> None:

    # (end BX value, name of TTree in root file)
    BXinfo = [
        (32, "events"),  # standard 63 BX window
        (5, "events_36BX"),
        (-7, "events_24BX"),
        (-13, "events_18BX"),
        (-19, "events_12BX"),
    ][slice(0, 1) if config.fast_analysis else slice(None, None)]

    position = 1 if config.detector.position == "LEFT" else 0
    event_list = []
    BEE_analysis_multi_link_gif_v3.gothroughfile(
        input_path, event_list, config.scan.triggers, link_num=config.detector.links)

    # total events to process
    # x2 because two loops over all events (inefficient I know!)
    total = config.detector.links * config.scan.triggers * len(BXinfo) * 2
    count = 0
    for link_number in range(config.detector.links):
        output_path = f"{output_dir}Chamber{config.detector[f'chamber{link_number + 1}']}/HV{hv}/"
        root_file = output_path + "data.root"
        if not converted(config, root_file):
            try:
                os.makedirs(output_path)
            except:
                os.system(f"rm {root_file}")
            with uproot.recreate(root_file) as file:
                for (BX_right_window, ttree) in BXinfo:
                    for item in event_list:
                        item.Link_info_list[link_number].run_all(
                            Chamber_type=config.detector.version,
                            trig_position=position,
                            offset_file=config.paths.offset_file,
                            left_window=-31,
                            rig_window=BX_right_window
                        )
                        count += 1
                        progress(count, total, suffix=f'converting run{config.run} hv{hv} data')
                    # create root TTree of the events and predefine structure
                    tree = {
                        "Strip": {
                            'Strip_num': [],
                            'Hit_position': [],
                            'HR_value': [],
                            'LR_value': [],
                            'Tdc_diff': [],
                            "X": [],
                            "Y": []
                        }
                    } | {
                        event_key: np.zeros(len(event_list)) for event_key in ("Event_num", "header_line", "L1T")
                    } | {
                        "BX_TDC": {
                            "hit_count": [],
                            "num": []}
                    } | {
                        "Channel": {
                            "hit_count": [],
                            "num": []
                        }
                    }

                    # run over all events
                    for event_id in range(len(event_list)):

                        Link_info_list_dict = event_list[event_id].Link_info_list[link_number].__dict__
                        # save information in event_list itself
                        for event_key in ("Event_num", "header_line"):
                            tree[event_key][event_id] = event_list[event_id].__dict__[
                                event_key]

                        # Level-1 Trigger
                        tree["L1T"][event_id] = Link_info_list_dict["L1T"]

                        # save BX TDC information in link info list (variable so must be with "append")
                        BX_TDC_args = np.nonzero(
                            Link_info_list_dict["BX_TDC_num"])[0]
                        tree["BX_TDC"]["hit_count"].append(np.array(
                            Link_info_list_dict["BX_TDC_num"])[BX_TDC_args])
                        # -31 since BX_TDC starts at -31
                        tree["BX_TDC"]["num"].append(BX_TDC_args - 31)

                        # save Channel information (variable so must be with "append")
                        hit_channel_args = np.nonzero(
                            Link_info_list_dict["Channel_hit_num"])[0]
                        tree["Channel"]["hit_count"].append(np.array(
                            Link_info_list_dict["Channel_hit_num"])[hit_channel_args])
                        tree["Channel"]["num"].append(hit_channel_args)

                        # combine strips of an event to list (variable so must be with "append")
                        Strips = [event_list[event_id].Link_info_list[link_number].Hit_info_list[i].__dict__ for i in range(
                            len(event_list[event_id].Link_info_list[link_number].Hit_info_list))]
                        # loop over all strip info
                        for strip_key in ("Strip_num", "Hit_position", "HR_value", "LR_value", "Tdc_diff"):
                            tree["Strip"][strip_key].append(
                                [strip[strip_key] for strip in Strips])

                        # get the X and Y coordinates of each hit
                        X, Y = [], []
                        for strip in Strips:
                            x, y = get_strip_position(
                                strip=strip["Strip_num"],
                                hit_position=strip["Hit_position"],
                                cham_type=config.detector.version,
                                trig_position=config.detector.position,
                            )
                            X.append(x / 10)  # divide by 10 to convert to cm
                            Y.append(y / 10)
                        tree["Strip"]["X"].append(X)
                        tree["Strip"]["Y"].append(Y)

                        count += 1
                        progress(count, total, suffix=f'converting run{config.run} hv{hv} data')

                    for nested_object in ("Strip", "Channel", "BX_TDC"):
                        tree[nested_object] = ak.zip(tree[nested_object])
                        # ak.zip the nested info of the strips
                    file[ttree] = tree
        else:
            progress(
                total * 0.5 * (link_number + 1),
                total,
                suffix=f"converting run{config.run} hv{hv} data")


def main_conversion(
    config: DotMap
) -> bool:

    any_failed_plots = False
    # loop over all the high voltage points (from 1 to length of HVpoints)
    width = os.get_terminal_size().columns
    print("\n\x1b[1;49;33m", '-' * (width // 4),
          "converting & plotting data of high voltage points", '-' * (width // 4), "\x1b[0m\n")

    for i in range(1, 1 + config.scan.HVpoints):
        print("\n\x1b[0;49;33m" + f"running analysis for high voltage point {i}" + "\x1b[0;49;34m")
        input_path = find_data(config.paths.data, config.run, i)
        convert(config, input_path, config.paths.output, i)

        print("\x1b[0;49;34m" + "plotting..." + "\x1b[0m")
        if not config.fast_analysis:
            failed_plots = main_plots(config, i)
        else:
            failed_plots = fast_plots(config, i)

        if not failed_plots:
            print("\x1b[0;49;32m" +
                f"plotting finished and saved in {config.paths.output}ChamberXXX/HV{i}" + "\x1b[0m")
        any_failed_plots = any_failed_plots | failed_plots
    return any_failed_plots
