#!/usr/bin/env python
# coding: utf-8

import numpy as np
from re import S
from pickle import TRUE
from asyncio import events
from ast import Num
import math
import os
import pandas as pd
import warnings
warnings.simplefilter(action='ignore', category=FutureWarning)

import util.offset_determ as offset_determ
from util.read_config import read_config

default_config = read_config()

offset_db = None
# Read Strip length
re = pd.read_csv(default_config.paths.profile_file)

strip_0_to_47_length = []
for i in range(48):
    strip_0_to_47_length.append(re.iloc[i][2] / 10)


def channel2position(Strip_num, HR_time, LR_time, HR_offset, LR_offset, Chamber_type=3):
    speed = 29.9792 * 0.572  # cm/ns
    hit_position = strip_0_to_47_length[Strip_num] / 2 - (
        (HR_time + HR_offset) - (LR_time + LR_offset)) * speed / 2

    return hit_position


class strip_cluster():
    def __init__(self, dev, strip, tdc, time_diff):
        self.fpga_id = dev
        self.strip = strip
        self.tdc_info = tdc
        # The difference between the direct strip channel timestamp and the return strip channel timestamp (16 bits).
        self.time_diff = time_diff


class Hit_info():
    """
    hit infomation in each event
    each object refers to one hit
    """

    # def __init__(self,Strip_num,Hit_position,HR_value,LR_value):
    def __init__(self):
        """This is initial funciton"""
        self.Strip_num = -1  # Strip number of this hit，initial value: -1
        self.Hit_position = -1  # Distance from LR of this hit, initial value: -1
        self.HR_value = 0  # tdc value of HR
        self.LR_value = 0  # tdc value of LR
        self.Tdc_diff = 0       # HR-LR
        self.HR_value_raw = "0"  # tdc value of HR
        self.LR_value_raw = "0"
# 新增每个TDC的信息


class tdc_info():
    """
    tdc infomation in each event
    """
    # def __init__(self,Strip_num,Hit_position,HR_value,LR_value):

    def __init__(self):
        """This is initial funciton"""
        self.tdc_num = 0
        self.data_tdc_0 = 0
        self.FPGA_num = -1
        self.ch = -1
        self.strip_num = -1
        self.LR_flag = None
        self.HR_flag = None
        self.timestamp = -1
        self.timestampBX = -1000000000
        self.caliBX = -1000000000
        self.caliBX_new = -1000000000
        self.delay = -1


class Link_info():
    """
    Every event is not merely one link in DAQ file
    link information format is the same
    it is described in detail in Data format figure
    """

    def __init__(self):
        """This is initial funciton"""
        self.Num_of_data = 0            # number of tdc data in this event
        """type:int"""
        self.Num_of_Clusterinfo = 0  # number of tdc cluster in this event
        """type:int"""
        self.Tdc_data = []  # store all tdc data in this event
        """type:list"""
        self.Clusterinfo = [[]for i in range(124)]  # cluster information
        # self.RefNum = ""            #refer to the cooresponding reference Event number（用于emulator取数分析）
        # self.Fitflag = 0            #Firtflag = 1 refer to this event successfully fits with some reference event(用于emulator 取数分析)
        # 3*34 channel ,refer to number of hit on each channel
        self.Channel_hit_num = [0] * 102
        """type: list[]*102"""
        self.Channel_tdc_info = [[]
                                 for i in range(102)]  # tdc data on each channel,
        self.Channel_tdc_info_raw = [[] for i in range(102)]
        """type:list[[] for i in range(102)]"""
        self.Fire_flag = None  # Mark this Event is fired, if number of hitted strip exceed the setting limit.
        """Type:bool"""
        self.Retrigger_flag = None      # "1" refers to at least one channel in this event has retrigger issue; "retrigger" here means there are more than one tdc value on one channel
        """Type:bool"""
        self.Hit_info_list = [
        ]  # 20220605 event中每个有效hit(has both LR and HR)的信息，列表中的每个元素应该是一个Hit_info object
        """type:Hit_info object"""
        # self.Hit_groupby = []            #20220605 长度应与Hit_info_list完全相同，并一一对应！！！作为group的分类标准， 比如说以HR的范围作为分类标注、那么每个元素的值可以为 Hit_info_list[i],HR_value//5
        # self.Overflow = False            #20220606 "True" refers to calculated position using the LR and HR is more than 150 or less than 0, in other words, it exceed chamber boundary
        # 20220613 48 Strip,index means Strip number, elements refer to valid hit(0<position<~150cm) number in each strip
        self.Strip_hit_nums = [0] * 48
        """type: []*48"""
        self.__hit_generated_flag = False  # private property, '1' refers to tdc-to-position convert position is finished
        self.L1T = 0           # calibrated BX value corresponding with L1A signal, in ns
        self.calibrationBCN_list = []
        self.BX_TDC_num = [0] * 63
        self.event_tdc_info = []
        # True indicates the sirip cluster feature is enabled, Default value is False.
        self.strip_cluster_flag = False
        self.strip_cluster_list = []  # List to store the strip_cluster()
        self.tdc_data_per_bx = [[] for i in range(63)]
        self.left_window = -31
        self.rig_window = 32

    def ch_info_generator(self, left_window, rig_window):
        self.left_window = left_window
        self.rig_window = rig_window
#         print(self.left_window)
#         print(self.rig_window)
        self.Channel_hit_num = [0] * 102
        self.Channel_tdc_info = [[]
                                 for i in range(102)]  # tdc data on each channel,
        self.Channel_tdc_info_raw = [[] for i in range(102)]

        window_size_cali = self.rig_window - self.left_window
        start_window = self.left_window + 31
        for i in range(start_window, start_window + window_size_cali):
            if self.tdc_data_per_bx[i] != []:
                for item in self.tdc_data_per_bx[i]:
                    channel_num = (int(item[0], 16) // 4) * \
                        34 + int(item[0:2], 16) % 64
                    if (channel_num >= 102):
                        print("CHANNEL NUMBER ERROR:", channel_num)
                    else:
                        self.Channel_hit_num[channel_num] += 1
                        self.Channel_tdc_info[channel_num].append(
                            int(item[2:8], 16) * 2.5 / 256)  # in ns
                        self.Channel_tdc_info_raw[channel_num].append(item)

    def retrigger_check(self):
        """check the Retrigger issue in this event"""
        Retrigger_flag = False
        for item in self.Channel_hit_num:
            if (item > 1):
                Retrigger_flag = True
                break
        self.Retrigger_flag = Retrigger_flag

    def fire_check(self, num_of_strip=12):
        """
        check the Fire issue in this event 

        Parameters
        ----------
        num_of_strip : int : number of hitted strips limit, if hitted strip in one event exceeds the number, it is a Fire event

        """
        num_of_hitted_strip = 0         # number of hitted strips, if there are more than 1 hits on one strip, it is considered as one hitted strip as well.
        if (self.__hit_generated_flag == 0):
            self.hit_info_generator

        for item in self.Strip_hit_nums:
            if item > 0:
                num_of_hitted_strip += 1

        if (num_of_hitted_strip > num_of_strip):
            self.Fire_flag = True
        else:
            self.Fire_flag = False

    def hit_info_generator(self, offset_file, Chamber_type, trig_position):
        self.Hit_info_list = []
        """generate hit_info_list"""
        global offset_db
        if (offset_db == None):
            offset_db = offset_determ.offset_derterm(offset_file)
            offset_list = offset_db
        else:
            offset_list = offset_db
        # print("****** Offset in Each channel ******")
        # for i in range(48):
        #     print("Strip",i,",","HR offset:",offset_list[i][0],"ns, LR offset:",offset_list[i][1],"ns")

        # "1" refer to this Strip have both HR and LR tdc value
        Strip_with_both_side_tdc = [0] * 48
        if (not self.strip_cluster_flag):  # strip cluster is not enabled
            for i in range(0, 48):
                if (trig_position == 0):  # LEFT
                    if i in range(0, 16):  # FPGA2    Strip 0~ Strip 15
                        LR_channel = 68 + 31 - i
                        HR_channel = 68 + i
                        Strip_with_both_side_tdc[i] = 1 if ((self.Channel_hit_num[i + 68] > 0) and (
                            self.Channel_hit_num[68 + 31 - i]) > 0) else 0  # HR = i -32 +68 ; LR = 68 + 31 -(i-32)

                    elif i in range(16, 32):  # FPGA1    Strip 16 ~ Strip 32
                        LR_channel = 34 + 47 - i
                        HR_channel = i - 16 + 34
                        Strip_with_both_side_tdc[i] = 1 if ((self.Channel_hit_num[i - 16 + 34] > 0) and (
                            self.Channel_hit_num[34 + 47 - i]) > 0) else 0  # HR = i -16 +34 ; LR = 34 + 31 -(i-16)

                    elif i in range(32, 48):  # FPGA0    Strip 32 ~ Strip 48
                        LR_channel = 63 - i
                        HR_channel = i - 32
                        Strip_with_both_side_tdc[i] = 1 if ((self.Channel_hit_num[i - 32] > 0) and (
                            self.Channel_hit_num[63 - i]) > 0) else 0  # HR = i -32 ; LR = 31 -(i-32)
                elif (trig_position == 1):  # RIGHT

                    if i in range(0, 16):  # FPGA0    Strip 0~ Strip 15
                        LR_channel = 16 + i
                        HR_channel = 15 - i
                        Strip_with_both_side_tdc[i] = 1 if ((self.Channel_hit_num[HR_channel] > 0) and (
                            self.Channel_hit_num[LR_channel]) > 0) else 0  # HR = i -32 +68 ; LR = 68 + 31 -(i-32)

                    elif i in range(16, 32):  # FPGA1    Strip 16 ~ Strip 32
                        LR_channel = 34 + i
                        HR_channel = 34 + 31 - i
                        Strip_with_both_side_tdc[i] = 1 if ((self.Channel_hit_num[HR_channel] > 0) and (
                            self.Channel_hit_num[LR_channel]) > 0) else 0  # HR = i -16 +34 ; LR = 34 + 31 -(i-16)

                    elif i in range(32, 48):  # FPGA2    Strip 32 ~ Strip 48
                        LR_channel = 68 + i - 16
                        HR_channel = 68 + 47 - i
                        Strip_with_both_side_tdc[i] = 1 if ((self.Channel_hit_num[HR_channel] > 0) and (
                            self.Channel_hit_num[LR_channel]) > 0) else 0  # HR = i -32 ; LR = 31 -(i-32)

                if (Strip_with_both_side_tdc[i] == 1):
                    HR_offset = offset_list[i][0]
                    LR_offset = offset_list[i][1]
                    for j in range(self.Channel_hit_num[HR_channel]):
                        for k in range(self.Channel_hit_num[LR_channel]):
                            Hit_position = channel2position(
                                i, self.Channel_tdc_info[HR_channel][j], self.Channel_tdc_info[LR_channel][k], HR_offset, LR_offset, Chamber_type=Chamber_type)
                            if (Hit_position > 0 and Hit_position < strip_0_to_47_length[i] and Chamber_type == 3) or\
                                    (Hit_position > 0 and Hit_position < strip_0_to_47_length[i] and Chamber_type == 4):
                                # 20220613，calculate valid hit numbers in each strip
                                self.Strip_hit_nums[i] += 1
                                self.Hit_info_list.append(Hit_info())
                                self.Hit_info_list[-1].Strip_num = i
                                self.Hit_info_list[-1].Hit_position = Hit_position
                                self.Hit_info_list[-1].HR_value = self.Channel_tdc_info[HR_channel][j]
                                self.Hit_info_list[-1].LR_value = self.Channel_tdc_info[LR_channel][k]
                                self.Hit_info_list[-1].Tdc_diff = self.Channel_tdc_info[HR_channel][j] - \
                                    self.Channel_tdc_info[LR_channel][k]
                                self.Hit_info_list[-1].HR_value_raw = self.Channel_tdc_info_raw[HR_channel][j]
                                self.Hit_info_list[-1].LR_value_raw = self.Channel_tdc_info_raw[LR_channel][k]
        else:  # strip cluster is enabled
            for item in (self.strip_cluster_list):
                Strip_num = item.fpga_id * 16 + (item.strip - 34)
                HR_time = item.tdc_info * 2.5 / 256
                timediff = item.time_diff * 2.5 / 256
                HR_offset = offset_list[47 - Strip_num][0]
                LR_offset = offset_list[47 - Strip_num][1]

                Hit_position = channel2position(
                    (47 - Strip_num), HR_time, HR_time + timediff, HR_offset, LR_offset, Chamber_type=Chamber_type)

                if (Hit_position > 0 and Hit_position < strip_0_to_47_length[47 - Strip_num] and Chamber_type == 3) or\
                        (Hit_position > 0 and Hit_position < strip_0_to_47_length[47 - Strip_num] and Chamber_type == 4):
                    # 20220613，calculate valid hit numbers in each strip
                    self.Strip_hit_nums[47 - Strip_num] += 1
                    self.Hit_info_list.append(Hit_info())
                    self.Hit_info_list[-1].Strip_num = 47 - Strip_num
                    self.Hit_info_list[-1].Hit_position = Hit_position
                    self.Hit_info_list[-1].HR_value = HR_time
                    self.Hit_info_list[-1].LR_value = None
                    self.Hit_info_list[-1].Tdc_diff = timediff
                    self.Hit_info_list[-1].HR_value_raw = None
                    self.Hit_info_list[-1].LR_value_raw = None

        self.__hit_generated_flag = True

#     def tdc_info_generator(self):
#         for i in range(len(self.Tdc_data)):
#             self.event_tdc_info.append(tdc_info())
#             data_tdc=self.Tdc_data[i]
#             self.event_tdc_info[-1].tdc_num=i
#             self.event_tdc_info[-1].data_tdc_0=data_tdc
#             self.event_tdc_info[-1].data_tdc_0=data_tdc
#             self.event_tdc_info[-1].FPGA_num=bin(int(data_tdc[0:2],16))[2:].zfill(8)[:2]
#             self.event_tdc_info[-1].ch=int(bin(int(data_tdc[0:2],16))[2:].zfill(8)[2:],2)
#             self.event_tdc_info[-1].timestamp=int(data_tdc[2:8],16)*2.5/256#ns
#             self.event_tdc_info[-1].timestampBX=int(int(data_tdc[2:8],16)/2560)#BX
# #             self.event_tdc_info[-1].caliBX = self.calibrationBCN_list[i] #每一帧开头的那16位，是一个BX量级的数值
# #             self.event_tdc_info[-1].delay = self.calibrationBCN_list[i]-int(int(data_tdc[2:8],16)/2560) #caliBX和TDC数值的一个差值(换算到BX量级)
#             if 16<=self.event_tdc_info[-1].ch <=31:
#                 self.event_tdc_info[-1].LR_flag=True
#             if 0<=self.event_tdc_info[-1].ch <=15:
#                 self.event_tdc_info[-1].HR_flag=True
#             if 0<=self.event_tdc_info[-1].ch <=15:
#                 if self.event_tdc_info[-1].FPGA_num=="00":
#                     self.event_tdc_info[-1].strip_num = 47-(15 - self.event_tdc_info[-1].ch)#strip number
#                 elif self.event_tdc_info[-1].FPGA_num=="01":
#                     self.event_tdc_info[-1].strip_num = 47-(31 - self.event_tdc_info[-1].ch)#strip number
#                 elif self.event_tdc_info[-1].FPGA_num=="10":
#                     self.event_tdc_info[-1].strip_num = 47-(47 - self.event_tdc_info[-1].ch)#strip number

#             if 16<=self.event_tdc_info[-1].ch <=31:
#                 if self.event_tdc_info[-1].FPGA_num=="00":
#                     self.event_tdc_info[-1].strip_num = 47 - self.event_tdc_info[-1].ch+16#strip number
#                 elif self.event_tdc_info[-1].FPGA_num=="01":
#                     self.event_tdc_info[-1].strip_num = 47 - self.event_tdc_info[-1].ch#strip number
#                 elif self.event_tdc_info[-1].FPGA_num=="10":
#                     self.event_tdc_info[-1].strip_num = 47 - self.event_tdc_info[-1].ch-16#strip number\

    def run_all(self, trig_position, offset_file="No_offset.xlsx", Chamber_type=3, left_window=-31, rig_window=32):
        self.ch_info_generator(left_window, rig_window)
        self.retrigger_check()
        self.hit_info_generator(offset_file, Chamber_type, trig_position)
        self.fire_check()
#         self.tdc_info_generator()


class Event():
    """
    Event in DAQ file
    it is described in detail in Data format figure
    """

    def __init__(self):
        """This is initial funciton"""
        self.Event_num = 0  # event index in this file
        """Type:int"""
        self.header_line = 0  # line number of "deadbeef" in the file
        """type:int"""
        self.Tx_link_length = [0] * 8
        self.Rx_link_length = [0] * 8
        self.Link_info_list = []


def file_format_check(file_path):  # @@Weizhuo Diao

    def frame_check(data_buffer, runtime, event_length_error_buffer,
                    trigger_number_error_buffer, input_length_error_buffer, data_previous, trigger_previous):
        input_header_position = 0
        ouput_header_position = 0
        a = 0
        output_header_buffer = []
        # beefdead的check
        if data_buffer[-1][4:12] != "beefdead":
            beef_error_buffer.append(data_buffer)
            beef_error_buffer.append(runtime)
            a = a + 1
        # 倒数第二行的检查
        if data_buffer[-2] != '0000000000000000':
            t = data_buffer.copy
            BEB_error_buffer.append(data_buffer[-2])
            BEB_error_buffer.append(t)
            BEB_error_buffer.append('-----')
            a = a + 1
        if a == 0:  # 如果基本格式正确，再进行下一行判断
            # 事例长度检查，指的是Event Length
            format_length = int(data_buffer[1][12:16], 16)  # 把14：16改成了12：16！！！
            actual_length = len(data_buffer)
            if actual_length != format_length:
                event_length_error_buffer.append(data_buffer)
                a = a + 1
            # 触发号检查
            trigger_present = int(data_buffer[0][8:16], 16)
        #     print(trigger_present)
            if runtime > 0:
                delta_trigger = trigger_present - trigger_previous
                if delta_trigger != 1:
                    trigger_number_error_buffer.append(delta_trigger)
                    trigger_number_error_buffer.append(data_previous)
                    trigger_number_error_buffer.append(data_buffer)
                    trigger_number_error_buffer.append(
                        '----------------------')
                    a = a + 1
            # orbit_number号检查
    #             orbnum_present = int(data_buffer[1][0:6],16)
    #             if runtime > 0 :
    #                 delta_orbnum = orbnum_present - orbnum_previous
    #                 if delta_orbnum != 1 and delta_orbnum != 2:
    #                     orbnum_error_buffer.append(delta_orbnum)
    #                     orbnum_error_buffer.append(data_previous)
    #                     orbnum_error_buffer.append(data_buffer)
    #                     orbnum_error_buffer.append('----------------------')
    #                 else :
    #                     orbnum_error_buffer.append(delta_orbnum)
    #                     orbnum_error_buffer.append('0')
    #                     orbnum_error_buffer.append('0')
    #                     orbnum_error_buffer.append('----------------------')
            # input length 检查指的是Link 1,2,3.....
            link_1_to_8_length = bin(int(data_buffer[2][2:16], 16))[
                2:].zfill(56)
            link_1_to_8_leg = [0, 0, 0, 0, 0, 0, 0, 0]
            for i in range(8):
                link_1_to_8_leg[i] = int(
                    int(link_1_to_8_length[(7 * i):(7 * i + 7)], 2))
        #     print(link_1_to_8_length)
            link_1_to_8_leg = list(reversed(link_1_to_8_leg))

            all_8_linklength = sum(link_1_to_8_leg)

            input_header_position = 0
            ouput_header_position = 0

            for i in range(len(data_buffer)):
                if data_buffer[i][0:2] == 'fa':
                    input_header_position = i
                if data_buffer[i][0:4] == 'ffff':
                    ouput_header_position = i
            delta_actual_length_64bit = ouput_header_position - input_header_position - 1
            if (delta_actual_length_64bit != all_8_linklength):
                input_length_error_buffer.append(all_8_linklength)
                input_length_error_buffer.append(delta_actual_length_64bit)
                input_length_error_buffer.append(data_buffer)
                input_length_error_buffer.append(runtime)
                input_length_error_buffer.append('----------------------')
                a = a + 1
            # 新加output windows的判断
            input_header_position_o = 0
            output_header_position_o = 0
            for i in range(len(data_buffer)):
                if data_buffer[i][0:4] == 'ffff':
                    input_header_position_o = i
                    output_header_buffer = data_buffer[i]
                if data_buffer[i][4:12] == 'beefdead':
                    output_header_position_o = i
            delta_actual_length_64bit_o = output_header_position_o - input_header_position_o - 2

            link_1_to_8_length_o = bin(int(output_header_buffer[4:16], 16))[
                2:].zfill(48)
            link_1_to_8_leg_o = [0, 0, 0, 0, 0, 0, 0, 0]
            for i in range(8):
                link_1_to_8_leg_o[i] = int(
                    int(link_1_to_8_length_o[(6 * i):(6 * i + 6)], 2))
        #     print(link_1_to_8_length)
            link_1_to_8_leg_o = list(reversed(link_1_to_8_leg_o))

            all_8_linklength_o = sum(link_1_to_8_leg_o)
            if (delta_actual_length_64bit_o != all_8_linklength_o):
                output_length_error_buffer.append(all_8_linklength_o)
                output_length_error_buffer.append(delta_actual_length_64bit_o)
                output_length_error_buffer.append(data_buffer)
                output_length_error_buffer.append(runtime)
                output_length_error_buffer.append('----------------------')
                a = a + 1
            # 新加以及判断，判断Evt_type的状态并且对比Input windows的数值是否和Link length相等
            Evt_type = bin(int(data_buffer[1][11:12], 16))[2:].zfill(4)
        #     print(Evt_type)
            if Evt_type == "1100":  # 说明没有进行零压缩这次分析
                # 判断input windows
                input_windows = bin(int(data_buffer[1][6:9], 16))[2:]
                input_windows_neg = input_windows[0:5]
                input_windows_pos = input_windows[5:10]
                input_windows_bx = (
                    int(input_windows_neg, 2) + int(input_windows_pos, 2) + 1) * 2
                input_windows_lis.append(input_windows_bx)
        #         print(input_windows_lis)
                if input_windows_bx != delta_actual_length_64bit:
                    input_windows_bx_error.append(input_windows_bx)
                    input_windows_bx_error.append(delta_actual_length_64bit)
                    input_windows_bx_error.append(all_8_linklength)
                    input_windows_bx_error.append(data_buffer)
                    input_windows_bx_error.append(runtime)
                    input_windows_bx_error.append('----------------------')
                    a = a + 1
                # 判断output windows
                output_windows = bin(int(data_buffer[1][9:11], 16))[
                    2:].zfill(8)
        #         print(output_windows)
                output_windows_neg = output_windows[0:4]
                output_windows_pos = output_windows[4:8]
                # output_windows_bx = (int(output_windows_neg,2)+int(output_windows_pos,2)+1)*2
                output_windows_bx = int(
                    output_windows_neg, 2) + int(output_windows_pos, 2) + 1
                output_windows_lis.append(output_windows_bx)
                if output_windows_bx != delta_actual_length_64bit_o:
                    output_windows_bx_error.append(output_windows_bx)
                    output_windows_bx_error.append(delta_actual_length_64bit_o)
                    output_windows_bx_error.append(all_8_linklength_o)
                    output_windows_bx_error.append(data_buffer)
                    output_windows_bx_error.append(runtime)
                    output_windows_bx_error.append('----------------------')
                    a = a + 1
        #     if beef_error_buffer == [] and event_length_error_buffer==[] and trigger_number_error_buffer==[] and input_length_error_buffer==[] and output_length_error_buffer and input_windows_bx_error==[] and output_windows_bx_error ==[]:
        if a == 0:
            return 1
        else:
            return 0

    myPath = file_path
    # mypath就是文件路径
    fsize_B = os.path.getsize(myPath)
    # 将数据文件大小转为bit，按64bit每行计算多少行
    flines_64b = int(fsize_B * 8 / 64)
    # 一个事例至少包括Header0，Header1，link1或link2 header，data或angle，尾巴，即5*64bit，所以一个数据文件中做多包含math.ceil(flines_64b/5)
    # 个事例，处理时按事例读取文件，估算事例数evts_evaluate/每批处理数据数 = 循环最大所需次数
    fo = open(myPath, "rb")
    header_finder = 0
    event_reject = 0
    for i in range(0, flines_64b):
        data_reject = fo.read(8)  # fo.read(x),x指字节
        if data_reject.hex()[0:8] == 'deadbeef':
            header_finder = header_finder + 1
    runtime_maxreq = header_finder
    # print(runtime_maxreq)
    fo.close()

    fo = open(myPath, "rb")
    data_buffer = []
    temp_buffer = []
    total_data_buffer = []
    event_length_error_buffer = []
    trigger_number_error_buffer = []
    orbnum_error_buffer = []
    input_length_error_buffer = []
    temp_event_buffer = []
    temp_event_buffer_o = []
    data_previous = 0
    total_event_cnt = 0
    l1a_error = 0
    link_1_to_8_databuf = []
    trigger_previous = 0
    orbnum_previous = 0
    trigger_present = 0
    Evt_type = 0
    input_windows = 0
    input_windows_neg = 0
    input_windows_pos = 0
    input_windows_bx = 0
    local_cnt_error = []
    input_windows_bx_error = []
    input_windows_lis = []
    output_length_error_buffer = []
    link_1_to_8_databuf_o = []
    BEB_error_buffer = []
    output_windows_lis = []
    output_windows_bx_error = []
    true_event = 0
    true_event_buffer = []
    beef_error_buffer = []
    fr = 0
    for i in range(0, runtime_maxreq - 1):
        event_cnt = 0
        data_64b = 1
        head_found = 0
        while (data_64b and (event_cnt < 2)):
            data_64b = fo.read(8)  # fo.read(x),x指字节
    #         print(data_64b.hex())
            if data_64b:  # 文件读完后，data_64b不真，停止存入data_buffer
                if data_64b.hex()[0:8] == 'deadbeef':
                    head_found = 1
                    event_cnt = event_cnt + 1
            if head_found == 1 and event_cnt < 2:
                temp_buffer.append(data_64b.hex())
        data_buffer = temp_buffer.copy()  # 存入一次事例

        if i == 0:
            trigger_previous = int(data_buffer[0][8:16], 16)
            orbnum_previous = int(data_buffer[1][0:6], 16)
        #         fr=frame_check(data_buffer,i,event_length_error_buffer,trigger_number_error_buffer,
        #                 orbnum_error_buffer,input_length_error_buffer,
        #                 data_previous,orbnum_previous,trigger_previous)
        fr = frame_check(data_buffer, i, event_length_error_buffer, trigger_number_error_buffer,
                         input_length_error_buffer, data_previous, trigger_previous)
    #         print(fr)
        if fr == 1:
            true_event = true_event + 1
        else:
            true_event = true_event
        fo.seek(-8, 1)
        data_previous = data_buffer.copy()
        trigger_previous = int(data_buffer[0][8:16], 16)
    #         orbnum_previous = int(data_buffer[1][0:6],16)
        temp_buffer.clear()
    # print(true_event)
    if true_event == runtime_maxreq - 1:
        return True
    else:
        return False


def bit_conv(param):
    if (param == 0):
        return "000"
    elif (param == 1):
        return "001"
    elif (param == 2):
        return "010"
    elif (param == 3):
        return "011"
    elif (param == 4):
        return "100"
    elif (param == 5):
        return "101"
    elif (param == 6):
        return "110"
    elif (param == 7):
        return "111"
    else:
        return "error"


def gothroughfile(file_path, event_list, number_of_event, event_type=Event, starting_event=0, rx_window_middle=31, rx_window_cut_lef=31, rx_window_cut_rig=32, link_num=1):
    """
    go through the data file pick useful information and fill them into event_list.


    Parameters
    ----------
    file_path : str: path of DAQ file 

    Event_list: list: Element is Event(); New empty list should be put here.

    number_of_event: int: the number of events for check

    event_type :type : it must be Event() class or subclass of Event(),  subclass is supposed to keep all the Atributes and Methods Event() has, and add some new ones.

    starting_event: int: it refers to number of the first event to be analysed

    rx_window_middle: int: The general is 31.For non-zero compression, the input window typically has 63 data frames.This parameter is invalid when data is zero compressed!!!

    rx_window_cut_lef: int: Base on the rx_window_middle, select n data frames on the left.This parameter is invalid when data is zero compressed!!!

    rx_window_cut_rig: int: Base on the rx_window_middle, select n data frames on the right.This parameter is invalid when data is zero compressed!!!

    link_num: The file currently analyzed has several links open, and the default setting is 1

    Returns
    -------

    """
    Data_str_arr = []  # string list storing each line of DAQ file
    NumOfEvent = -1  # number of Events in the file
    EOD_flag = 0                # end of data file flag
    No_more_data_flag = 0       # starting_event is more than total Event number
    f = open(file_path, "rb")
    fsize = os.path.getsize(file_path)
    for i in range(0, fsize):
        lines = f.read(8)  # read 8 bytes, 64bits
        if lines:
            Data = lines.hex()  # convert bytes to Str
            if ("deadbeef" in Data):
                NumOfEvent += 1
            if ((NumOfEvent >= starting_event) and (NumOfEvent < starting_event + number_of_event)):
                Data_str_arr.append(Data)

            elif (NumOfEvent == starting_event + number_of_event):
                break
        else:
            EOD_flag = 1
            if (NumOfEvent < starting_event):
                No_more_data_flag = 1
            break
    f.close()
    # get the number of all the events in the file
    # for i in range(0,len(Data_str_arr)):
    #     if("deadbeef" in Data_str_arr[i]):
    #         NumOfEvent = NumOfEvent + 1
    NumOfEvent_for_chk = 0
#     NumOfEvent_for_chk = number_of_event - EOD_flag           #handle the last incomplete Event
#     NumOfEvent_for_chk_bak = number_of_event - EOD_flag
    # NumOfEvent_for_chk = min(NumOfEvent-1,number_of_event)
    if EOD_flag == 1:
        NumOfEvent_for_chk = NumOfEvent - starting_event
        NumOfEvent_for_chk_bak = NumOfEvent - \
            starting_event  # handle the last incomplete Event
    else:
        NumOfEvent_for_chk = number_of_event  # handle the last incomplete Event
        NumOfEvent_for_chk_bak = number_of_event  # handle the last incomplete Event

    # main body of "GO THROUGH" process
    # Event_num = 0 # Event index
#     print(Data_str_arr)
    Event_num = starting_event  # 20220624
    BC0_resync_ch = [32, 33, 66, 67, 100, 101]
    for i in range(0, len(Data_str_arr)):
        if (NumOfEvent_for_chk > 0):
            if ("deadbeef" in Data_str_arr[i]):  # discard the last 1 event
                Event_num += 1  # Event号 20220614
                NumOfEvent_for_chk -= 1
                event_list.append(event_type())
                event_list[-1].Event_num = Event_num - 1  # Event num 从0开始
                event_list[-1].header_line = i
#                 Last_byte_of_Link_header = int(Data_str_arr[i+2][14:16],16)
                # print(Data_str_arr[i+2])
                Last_byte_of_Link_header = bin(
                    int(Data_str_arr[i + 2][2:], 16))[2:].zfill(56)
                # print(Last_byte_of_Link_header)
                for j in range(0, 7):
                    link_length = int(
                        Last_byte_of_Link_header[49 - 7 * j:56 - 7 * j], 2)
                    # first，append link 1 length. second append link2 length ....
                    event_list[-1].Tx_link_length[j] = link_length
                # print(event_list[-1].Tx_link_length)
                ##
                # event_list[-1].Tx_link1_length = (Last_byte_of_Link_header - 128) if (Last_byte_of_Link_header > 128) else Last_byte_of_Link_header

                # zero compression judge,if data has been zero_compressed,then input windows counldn't be chosen
                Evt_type = bin(int(Data_str_arr[i + 1][11:12], 16))[2:].zfill(4)
                link_num = link_num
                for k in range(0, link_num):  # 2 means there are only 2 links
                    length0 = event_list[-1].Tx_link_length[k]
                    event_list[-1].Link_info_list.append(Link_info())
                # INPUT FRAME
                    event_list[-1].Link_info_list[-1].L1T = int(Data_str_arr[i + 3 + sum(
                        event_list[-1].Tx_link_length[:k])][:4], 16) * 25  # in ns unit

                    if Evt_type == "1100":  # non_zero_compression
                        rx_window_middle_0 = 2 * rx_window_middle
                        rx_window_cut_lef_0 = 2 * rx_window_cut_lef
                        rx_window_cut_rig_0 = 2 * rx_window_cut_rig
                        length = rx_window_cut_lef_0 + rx_window_cut_rig_0
                        start_window = rx_window_middle_0 - rx_window_cut_lef_0

                    else:  # zero_compression or other situations
                        rx_window_middle_0 = 0
                        rx_window_cut_lef_0 = 0
                        length = length0
                        start_window = 0

    #                     length=event_list[-1].Tx_link1_length

                    # Check the  status of the strip cluster
                    for j in range(start_window, start_window + length, 2):
                        IsStrip = int(
                            Data_str_arr[i + 3 + j + sum(event_list[-1].Tx_link_length[:k])][6], 16) & 0b0011
                        if (IsStrip != 0):
                            event_list[-1].Link_info_list[-1].strip_cluster_flag = True
                            break
                    # The strip cluster feature is not enabled.
                    if (not event_list[-1].Link_info_list[-1].strip_cluster_flag):
                        for j in range(start_window, start_window + length, 2):
                            #                 for j in range(0,event_list[-1].Tx_link1_length,2):
                            BX = int(j / 2)
                            data_valid = int(
                                Data_str_arr[i + 3 + j + sum(event_list[-1].Tx_link_length[:k])][7], 16) & 0b0111
                            # 修改代码内容22.9.12_link2无效数据的判断
        #                     data_valid
                            calibrationBCN = int(
                                Data_str_arr[i + 3 + j + sum(event_list[-1].Tx_link_length[:k])][:4], 16)
                            event_list[-1].Link_info_list[-1].calibrationBCN_list.append(
                                calibrationBCN)
                            if (data_valid > 7):
                                print(
                                    Data_str_arr[i + 3 + j + sum(event_list[-1].Tx_link_length[:k])])
                                print("Data_valid error!!!")
                            if (bit_conv(data_valid)[0] == "1"):
                                #                                 event_list[-1].Link_info_list[-1].calibrationBCN_list.append(calibrationBCN)
                                event_list[-1].Link_info_list[-1].Num_of_data += 1
                                tdc_data = Data_str_arr[i + 3 + j +
                                                        sum(event_list[-1].Tx_link_length[:k])][8:16]
        #                         if tdc_data !="35555655":
                                event_list[-1].Link_info_list[-1].Tdc_data.append(
                                    tdc_data)
                                event_list[-1].Link_info_list[-1].tdc_data_per_bx[BX].append(
                                    tdc_data)
                                # channel number corresponding the above Tdc_data
                                channel_num = (
                                    int(tdc_data[0], 16) // 4) * 34 + int(tdc_data[0:2], 16) % 64

#                                 if (channel_num >= 102):
#                                     print("CHANNEL NUMBER ERROR:",channel_num)
#                                 else:
#                                     event_list[-1].Link_info_list[-1].Channel_hit_num[channel_num] += 1
#                                     event_list[-1].Link_info_list[-1].Channel_tdc_info[channel_num].append(int(tdc_data[2:8],16)*2.5/256) # in ns
#                                     event_list[-1].Link_info_list[-1].Channel_tdc_info_raw[channel_num].append(tdc_data)
                                if channel_num not in BC0_resync_ch:
                                    event_list[-1].Link_info_list[-1].BX_TDC_num[BX] = event_list[-1].Link_info_list[-1].BX_TDC_num[BX] + 1

                            if (bit_conv(data_valid)[1] == "1"):
                                #                                 event_list[-1].Link_info_list[-1].calibrationBCN_list.append(calibrationBCN)
                                event_list[-1].Link_info_list[-1].Num_of_data += 1
                                tdc_data = Data_str_arr[i + 4 + j +
                                                        sum(event_list[-1].Tx_link_length[:k])][0:8]
        #                         if tdc_data !="554d5555":
                                event_list[-1].Link_info_list[-1].Tdc_data.append(
                                    tdc_data)
                                event_list[-1].Link_info_list[-1].tdc_data_per_bx[BX].append(
                                    tdc_data)
                                # channel number corresponding the above Tdc_data
                                channel_num = (
                                    int(tdc_data[0], 16) // 4) * 34 + int(tdc_data[0:2], 16) % 64
#                                 if (channel_num >= 102):
#                                     print("CHANNEL NUMBER ERROR:",channel_num)
#                                 else:
#                                     event_list[-1].Link_info_list[-1].Channel_hit_num[channel_num] += 1
#                                     event_list[-1].Link_info_list[-1].Channel_tdc_info[channel_num].append(int(tdc_data[2:8],16)*2.5/256) # in ns
#                                     event_list[-1].Link_info_list[-1].Channel_tdc_info_raw[channel_num].append(tdc_data)
                                if channel_num not in BC0_resync_ch:
                                    event_list[-1].Link_info_list[-1].BX_TDC_num[BX] = event_list[-1].Link_info_list[-1].BX_TDC_num[BX] + 1

                            if (bit_conv(data_valid)[2] == "1"):
                                #                                 event_list[-1].Link_info_list[-1].calibrationBCN_list.append(calibrationBCN)
                                event_list[-1].Link_info_list[-1].Num_of_data += 1
                                tdc_data = Data_str_arr[i + 4 + j +
                                                        sum(event_list[-1].Tx_link_length[:k])][8:16]
        #                         if tdc_data !="55555555":
                                event_list[-1].Link_info_list[-1].Tdc_data.append(
                                    tdc_data)
                                event_list[-1].Link_info_list[-1].tdc_data_per_bx[BX].append(
                                    tdc_data)
                                # channel number corresponding the above Tdc_data
                                channel_num = (
                                    int(tdc_data[0], 16) // 4) * 34 + int(tdc_data[0:2], 16) % 64
#                                 if (channel_num >= 102):
#                                     print("CHANNEL NUMBER ERROR:",channel_num)
#                                 else:
#                                     event_list[-1].Link_info_list[-1].Channel_hit_num[channel_num] += 1
#                                     event_list[-1].Link_info_list[-1].Channel_tdc_info[channel_num].append(int(tdc_data[2:8],16)*2.5/256) # in ns
#                                     event_list[-1].Link_info_list[-1].Channel_tdc_info_raw[channel_num].append(tdc_data)
                                if channel_num not in BC0_resync_ch:
                                    event_list[-1].Link_info_list[-1].BX_TDC_num[BX] = event_list[-1].Link_info_list[-1].BX_TDC_num[BX] + 1
                    else:  # The strip cluster feature is enabled
                        for j in range(start_window, start_window + length, 2):
                            #                 for j in range(0,event_list[-1].Tx_link1_length,2):
                            BX = int(j / 2)
                            IsStrip = IsStrip = int(
                                Data_str_arr[i + 3 + j + sum(event_list[-1].Tx_link_length[:k])][6], 16) & 0b0011
                            data_valid = int(
                                Data_str_arr[i + 3 + j + sum(event_list[-1].Tx_link_length[:k])][7], 16) & 0b0111
                            calibrationBCN = int(
                                Data_str_arr[i + 3 + j + sum(event_list[-1].Tx_link_length[:k])][:4], 16)
                            event_list[-1].Link_info_list[-1].calibrationBCN_list.append(
                                calibrationBCN)
                            data = [None] * 2
                            time_diff = [None] * 2
                            # original data
                            data[0] = Data_str_arr[i + 3 + j +
                                                   sum(event_list[-1].Tx_link_length[:k])][8:16]
                            # original data
                            data[1] = Data_str_arr[i + 4 + j +
                                                   sum(event_list[-1].Tx_link_length[:k])][0:8]
                            # original data
                            time_diff[0] = Data_str_arr[i + 4 + j +
                                                        sum(event_list[-1].Tx_link_length[:k])][8:12]
                            # original data
                            time_diff[1] = Data_str_arr[i + 4 + j +
                                                        sum(event_list[-1].Tx_link_length[:k])][12:16]

                            for n in range(2):
                                # IsStrip = "bb" Data_valid = "bbb"
                                # when n = 0, take the right data
                                IsStripbit = (IsStrip >> (n)) & 1
                                DataValidbit = (data_valid >> (n + 1)) & 1
                                if (IsStripbit == 1):
                                    data_type = 1  # 1: Strip_cluster_data 2: Channel_data
                                elif (IsStripbit == 0 and DataValidbit == 1):
                                    data_type = 2
                                else:
                                    data_type = 0

                                if (data_type == 1):
                                    event_list[-1].Link_info_list[-1].Num_of_data += 2
                                    strip_cluster_dev = (
                                        int(data[1 - n][0], 16) & 0b1100) >> 2
                                    strip_cluster_strip = (
                                        int(data[1 - n][0:2], 16) & 0b00111111)
                                    strip_cluster_tdc = int(data[1 - n][2:8], 16)
                                    strip_cluster_timediff = int(
                                        time_diff[1 - n], 16)

                                    event_list[-1].Link_info_list[-1].strip_cluster_list.append(strip_cluster(
                                        strip_cluster_dev, strip_cluster_strip, strip_cluster_tdc, strip_cluster_timediff))
                                    event_list[-1].Link_info_list[-1].BX_TDC_num[BX] = event_list[-1].Link_info_list[-1].BX_TDC_num[BX] + 2

                                elif (data_type == 2):
                                    event_list[-1].Link_info_list[-1].Num_of_data += 1
                                    tdc_data = data[1 - n]
                                    event_list[-1].Link_info_list[-1].Tdc_data.append(
                                        tdc_data)
                                    # channel number corresponding the above Tdc_data
                                    channel_num = (
                                        int(tdc_data[0], 16) // 4) * 34 + int(tdc_data[0:2], 16) % 64
#                                     if (channel_num >= 102):
#                                         print("CHANNEL NUMBER ERROR:",channel_num)
#                                     else:
#                                         event_list[-1].Link_info_list[-1].Channel_hit_num[channel_num] += 1
#                                         event_list[-1].Link_info_list[-1].Channel_tdc_info[channel_num].append(int(tdc_data[2:8],16)*2.5/256) # in ns
#                                         event_list[-1].Link_info_list[-1].Channel_tdc_info_raw[channel_num].append(tdc_data)
                                    if channel_num not in BC0_resync_ch:
                                        event_list[-1].Link_info_list[-1].BX_TDC_num[BX] = event_list[-1].Link_info_list[-1].BX_TDC_num[BX] + 1

                if Data_str_arr[i + 3 + sum(event_list[-1].Tx_link_length)][:4] == "ffff":
                    Output_Link_header = (bin(
                        int(Data_str_arr[i + 3 + sum(event_list[-1].Tx_link_length)], 16))[2:].zfill(64))[16:]
                    right_len = sum(event_list[-1].Tx_link_length)
                # Ganrantrrn cluster info header("ffffxxxxxxxxxx,,,") is read correctly even if the input window header is wrong
                else:
                    print("Input windows header length error, Please check")
                    for line in range(126 * link_num + 10):
                        if Data_str_arr[i + 3 + line][:4] == "ffff":
                            Output_Link_header = (
                                bin(int(Data_str_arr[i + 3 + line], 16))[2:].zfill(64))[16:]
                            right_len = line
#                 print(Data_str_arr[i+3+sum(event_list[-1].Tx_link_length)])
#                 print(Output_Link_header)
                for j in range(0, 7):
                    link_length = int(Output_Link_header[42 - 6 * j:48 - 6 * j], 2)
                    event_list[-1].Rx_link_length[j] = link_length

                for j in range(0, link_num):
                    length0 = event_list[-1].Rx_link_length[j]
                    length1 = sum(event_list[-1].Rx_link_length[:j])
                    for k in range(0, length0):
                        # 修改成可以用的32
                        if (Data_str_arr[i + 4 + right_len + length1 + k][:8] != "00000000"):
                            event_list[-1].Link_info_list[j].Clusterinfo[2 * k].append(
                                Data_str_arr[i + 4 + right_len + length1 + k][:8])
                            event_list[-1].Link_info_list[j].Num_of_Clusterinfo += 1
                        # 修改成可以用的32
                        if (Data_str_arr[i + 4 + right_len + length1 + k][8:] != "00000000"):
                            event_list[-1].Link_info_list[j].Clusterinfo[2 * k + 1].append(
                                Data_str_arr[i + 4 + right_len + length1 + k][8:])
                            event_list[-1].Link_info_list[j].Num_of_Clusterinfo += 1
        else:
            break
    if (EOD_flag == 1):
        if (No_more_data_flag == 1):
            print("WARNING: starting_event parameter is more than total number of events")
#         else:
#             print("WARNNING: Only %d events are available!"%NumOfEvent_for_chk_bak)
#             print("Successfully read %d events"%NumOfEvent_for_chk_bak)
