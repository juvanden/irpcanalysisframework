#!/usr/bin/python
# coding: utf-8

"""
Script to plot the efficiency of the detector
"""

import uproot
import awkward as ak
import matplotlib.pyplot as plt
import numpy as np
import mplhep as hep
from scipy.optimize import curve_fit
import hist
from hist import Hist
from matplotlib.colors import LogNorm
import matplotlib.patches as patches
import sys
import os
from dotmap import DotMap
from shapely.geometry import Polygon
import matplotlib.colors as mcolors
import matplotlib.cm as cm

from util.strip_mapping import get_strip_trapezoid, get_pcb_outline

hep.style.use("CMS")

hatch_style = {
    'facecolor': 'none',
    'edgecolor': (0, 0, 0, 0.5),
    'linewidth': 0,
    'hatch': '///',
}

muon_window_peak_width = 50

# 2D Gaussian for fitting


def Gaussian2D(coords, a, x0, y0, sigma_x, sigma_y, c):
    x = coords[0]
    y = coords[1]
    return a * np.exp(-(x - x0)**2 / (2 * sigma_x**2) - (y - y0)**2 / (2 * sigma_y**2)) + c


def efficiency_window(config, output_path):
    # test with run33

    root_file = output_path + "data.root:events"

    with uproot.open(root_file) as file:
        events = file.arrays(
            ["Strip_X", "Strip_Y", "Strip_HR_value", "BX_TDC_num", "L1T"], library="ak")

    # cut on Muon Window
    h = (
        Hist.new.Var(np.linspace(-100, 1600, 300), name="hit_HR_time",
                     label="Hit time (HR) [ns]")
    ).Weight()
    h.fill(hit_HR_time=ak.flatten((events["Strip_HR_value"] - events["L1T"])))

    muon_windowpeak = h.axes[0].edges[np.argmax(h.values())]

    hit_mask = ((events["Strip_HR_value"] - events["L1T"]) > muon_windowpeak -
                25) & ((events["Strip_HR_value"] - events["L1T"]) < muon_windowpeak + 25)

    x = ak.flatten(events["Strip_X"][hit_mask])
    y = ak.flatten(events["Strip_Y"][hit_mask])

    x_mean = ak.mean(x)
    y_mean = ak.mean(y)
    x_sigma = np.sqrt(ak.var(x))
    y_sigma = np.sqrt(ak.var(y))

    # return a polygon area to check if hits are inside
    return (
        [x_mean - x_sigma, x_mean + x_sigma],
        [y_mean - y_sigma, y_mean + y_sigma]
    )


def efficiency(config, output_folder, poly, link_number):

    if config.fast_analysis:
        BX_cuts = (63,)
        root_finder = lambda HV, BX: f"{output_folder}/HV{HV}/data.root:events"
    else:
        BX_cuts = ((36, 24, 18, 12))
        root_finder = lambda HV, BX: f"{output_folder}/HV{HV}/data.root:events_{BX}BX"

    efficiency = {BX: np.zeros((4, config.scan.HVpoints)) for BX in BX_cuts}
    hit_rate = np.zeros(config.scan.HVpoints)

    for i in range(1, config.scan.HVpoints + 1):
        for BX in BX_cuts:
            root_file = root_finder(i, BX)

            with uproot.open(root_file) as file:
                events = file.arrays(
                    ["Strip_X", "Strip_Y", "Strip_HR_value", "L1T"], library="ak")

            # cut on Muon Window
            h = (
                Hist.new.Var(np.linspace(-100, 1600, 300), name="hit_HR_time",
                             label="Hit time (HR) [ns]")
            ).Weight()
            h.fill(hit_HR_time=ak.flatten(
                events["Strip_HR_value"] - events["L1T"]))

            muon_windowpeak = h.axes[0].edges[np.argmax(h.values())]

            muon_windowpeak_mask = ((events["Strip_HR_value"] - events["L1T"]) > muon_windowpeak -
                                    muon_window_peak_width / 2) & ((events["Strip_HR_value"] - events["L1T"]) < muon_windowpeak + muon_window_peak_width / 2)

            muon_windowpeak_noise_mask = ((events["Strip_HR_value"] - events["L1T"]) > muon_windowpeak +
                                          2 * muon_window_peak_width) & ((events["Strip_HR_value"] - events["L1T"]) < muon_windowpeak + 3 * muon_window_peak_width)

            hits_within_square_mask = ((events["Strip_X"] > poly[0][0]) &
                                       (events["Strip_X"] < poly[0][1]) &
                                       (events["Strip_Y"] > poly[1][0]) &
                                       (events["Strip_Y"] < poly[1][1]))

            n_events = len(events)

            n_mu = ak.sum((ak.any((hits_within_square_mask) &
                        (muon_windowpeak_mask), axis=-1)))

            n_noise = ak.sum((ak.any((hits_within_square_mask) &
                                     (muon_windowpeak_noise_mask), axis=-1)))

            #
            noise_hits = ak.sum((muon_windowpeak_noise_mask))
            eff = n_mu / n_events
            eff_noise = n_noise / n_events

            eff_corrected = (eff - eff_noise) / (1 - eff_noise)

            # save efficiency information
            efficiency[BX][0][i - 1] = config.scan.HVvalues[i - 1]
            efficiency[BX][1][i - 1] = eff
            efficiency[BX][2][i - 1] = eff_corrected
            efficiency[BX][3][i - 1] = eff_noise

            if BX == BX_cuts[0]:
                hit_rate[i - 1] = noise_hits / (muon_window_peak_width * (10 **
                                                   (-9)) * 12226.66 * 0.5 * n_events)  # in Hz

    # make 3 plots for efficiency, noise efficiency, and corrected efficiency:

    fig, ax = plt.subplots(figsize=(12, 12))
    hep.cms.label("Preliminary", data=True, rlabel=config.styling.cmslabel)

    colors = ["#3f90da", "#ffa90e", "#bd1f01", "#94a4a2", "#832db6",
              "#a96b59", "#e76300", "#b9ac70", "#717581", "#92dadd"]
    for i, BX in enumerate(BX_cuts):

        x0 = efficiency[BX][0]
        y0 = efficiency[BX][2]

        x_fit = x0[:np.argmax(y0)]
        y_fit = y0[:np.argmax(y0)]
        # limit x and y for fit until max y value

        def func1(x0, E1, L1, H1):
            return E1 / (1 + np.exp(L1 * (H1 - x0)))

        param_bounds = ([0, -np.inf, -np.inf], [1.00, np.inf, np.inf])
        popt, pcov = curve_fit(
            func1, x_fit, y_fit, [0.98, 0.01, 7000], bounds=param_bounds)

        WP = popt[2] - (np.log(1 / 0.95 - 1)) / popt[1] + 120
        WP_eff = 100 * func1(WP, popt[0], popt[1], popt[2])

        if i == 0:
            hit_rate_WP = hit_rate[np.argmin(abs(config.scan.HVvalues - WP))]

        ax.scatter(
            x0 / 1000, 100 * y0, color=colors[i], marker='x', label=f"RxLW = {BX} BX, plateau[%] = {popt[0]*100:.2f}, WP = {WP:.2f} V, Eff(WP)[%] = {WP_eff:.2f}%")

        x = np.linspace(int(min(x0)), int(max(x0)), 100)
        ax.plot(x / 1000, 100 * func1(x, popt[0],
                popt[1], popt[2]), color=colors[i])

    comment = "GIF++ test beam June 24\n" + "--------------------------------\n" + \
        f"chamber {config.detector[f'chamber{link_number + 1}']}\n" + \
        "1.4mm double gap iRPC\n" + \
        "FEB v2.3 Petiroc 2C\n" + \
        f"threshold ~ {config.styling.dac_threshold}fC (dac{config.styling.dac})\n"

    if config.styling.source != "off":
        comment += f"\u03B3-rate ~ {hit_rate_WP/1.8:.0f} Hz/cm²"
    else:
        comment += "\u03B3-source off"

    ax.text(6.1, 55, comment, fontsize=20, bbox=dict(
        facecolor='none', edgecolor='black', boxstyle='round,pad=1'))

    ax.set_xlabel("HV [kV]")
    ax.set_ylabel("Efficiency [%]")
    ax.set_ylim(0, 120)
    ax.set_xlim(config.scan.HVvalues[0] / 1000, config.scan.HVvalues[-1] / 1000)
    ax.set_yticks([0, 20, 40, 60, 80, 100])
    ax.grid()
    ax.plot([0, 10**4], [100, 100], linestyle="--", color="grey")
    ax.legend(loc='upper left', prop={
        'family': 'DejaVu Sans', 'weight': 'normal', 'size': 15, }, frameon=False)

    fig.savefig(f"{output_folder}/efficiency")
    plt.show()


def main_efficiency(config: DotMap):

    hv_working_point = 7
    # calculate efficiency for all links
    for link_number in range(config.detector.links):
        output_path = f"{config.paths.output}Chamber{config.detector[f'chamber{link_number + 1}']}/"

        poly = efficiency_window(config, output_path + f"HV{hv_working_point}/")

        efficiency(config, output_path, poly, link_number)
