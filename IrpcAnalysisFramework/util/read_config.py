# coding: utf-8

"""
Reading Configuration file of the RPC data analysis
"""

import os
import yaml
from dotmap import DotMap


def read_config(config_file="config.yml"):
    base_directory = os.environ["BASE"]
    analysis_directory = os.environ["ANALYSIS_BASE"]

    # load configuration file
    with open(f"{analysis_directory}/{config_file}") as file:
        config = DotMap(yaml.safe_load(file))

    # check if number of hvpoints correspond to the hv values
    if len(config.scan.HVvalues) != config.scan.HVpoints:
        ValueError(f"number of High voltage points does nog correspond to the number of values given in config.yml")
        quit()

    # check if all paths exist
    # if not and it is a directory make de directory
    for name, path in config.paths.items():
        path = base_directory + path
        if not os.path.exists(path):
            if path.endswith("/"):
                os.makedirs(path)
            else:
                ValueError(f"{name} does not have valid file or directory path in config.yml")
                quit()

        # add the absolute path to the config
        config.paths[name] = path

    return config


def print_config(config: DotMap) -> None:

    width = os.get_terminal_size().columns
    print("\n\x1b[1;49;33m", '-' * (width // 4),
          f"configuration used for the analysis", '-' * (width // 4), "\x1b[0m\n")

    for key, item in config.items():

        if type(item) != type(config):
            print("\x1b[1;49;35m" + f"{key}: " + "\x1b[1;49;32m" + f"{item}" + "\x1b[0m")
        else:
            print("\x1b[1;49;34m" + f"{key}" + "\x1b[0m")
            for sub_key, sub_item in item.items():
                if key == "paths":
                    print("\t" + "\x1b[1;49;35m" + f"{sub_key}:" + "\x1b[1;49;34m" + f" {sub_item}" + "\x1b[0m")
                else:
                    print("\t" + "\x1b[1;49;35m" + f"{sub_key}:" + "\x1b[1;49;32m" + f" {sub_item}" + "\x1b[0m")
