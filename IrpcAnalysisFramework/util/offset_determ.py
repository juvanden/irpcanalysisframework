# This script is used to read the configuration file(,xlsx) and get the offset of every channel.
# Data: 2023-07-10
# Author: Qingfeng.Hou

import sys
import os
# import shutil
import pandas as pd

def offset_derterm(offset_path):
    """
    read the configuration file (.xlsx)
    input: the configuration file path
    return: list with 48 unit, unit is a tuple (HR_offset, LR_offset)
    """
    try:
        data_frame = pd.read_excel(offset_path)
    except Exception as err:
    # handle the exception
        print("An exception occurred:", str(err))
        return list(zip([0]*48),[0]*48)
    
    HR_offset = data_frame.iloc[:, 1].tolist()
    LR_offset = data_frame.iloc[:, 2].tolist()

    if (len(HR_offset)!=48 or len(LR_offset) != 48):
        print("offset length error occured!!")
        return list(zip([0]*48),[0]*48)
    
    return list(zip(HR_offset,LR_offset)) 