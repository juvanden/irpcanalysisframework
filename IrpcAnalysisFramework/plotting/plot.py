#!/usr/bin/python
# coding: utf-8

"""
Scripts to plot data for n high voltage point
"""
import os
from dotmap import DotMap
from matplotlib.colors import LogNorm
import numpy as np
from hist import Hist
import mplhep as hep
import uproot
import awkward as ak
import matplotlib.pyplot as plt
import matplotlib.patches as patches
from matplotlib.ticker import MultipleLocator, FormatStrFormatter
import matplotlib.ticker as ticker
from shapely.geometry import Polygon
import matplotlib.colors as mcolors
import matplotlib.cm as cm

from util.strip_mapping import get_strip_trapezoid, get_pcb_outline

hep.style.use("CMS")

hatch_style = {
    'facecolor': 'none',
    'edgecolor': (0, 0, 0, 0.5),
    'linewidth': 0,
    'hatch': '///',
}


def converted(
    config: DotMap,
    path: str,
) -> bool:

    if not os.path.isfile(path):
        return False

    if not path.endswith(".root"):
        return False

    with uproot.open(path) as file:

        if (len(file.keys()) != 5) & (not config.fast_analysis):
            return False

        # prevent from skipping convertion if previous failed and ttree was never written out
        elif (len(file.keys()) == 0) & (config.fast_analysis):
            return False
    with uproot.open(path + ":events") as file:
        events = file.arrays(
            ["Strip_Strip_num"], library="ak")

    # prevent from skipping convertion if the events do not match triggers
    # TODO change this to give a warning and continue instead of doing the convertion again
    if len(events) != config.scan.triggers:
        return False
    elif len(ak.flatten(events["Strip_Strip_num"])) == 0:
        return False



    return True


def hit_heatmap(output_folder, status, comment):

    # open root file with data and extract information
    with uproot.open(output_folder + "data.root:events") as file:
        events = file.arrays(
            ["Strip_Strip_num", "Strip_Hit_position", "Channel_num", "Channel_hit_count", "BX_TDC_num", "BX_TDC_hit_count"], library="ak")

    h = (
        Hist.new.Int(1, 49, name="Strip_num", label="Strip Number")
        .Var(np.linspace(0, 150, 151), name="Hit_position", label="Hit position from low radius [cm]")
    ).Weight()

    try:
        h.fill(Strip_num=ak.flatten(events["Strip_Strip_num"]) + 1, Hit_position=ak.flatten(events["Strip_Hit_position"]))
    except:
        print("\x1b[1;49;31m" + "no hits in the scan, hit-heatmap.png plot will be empty" + "\x1b[0m")
    else:
        fig, ax = plt.subplots(figsize=(12, 12))

        cbar = h.plot2d(ax=ax, norm=LogNorm(), cmap='GnBu')
        ax.text(0.60, 0.92, comment, fontsize=20, ha='left', va='top', transform=ax.transAxes, bbox=dict(
            facecolor='white', alpha=0.6, edgecolor='black', boxstyle='round,pad=1'))
        ax.set_xticks(ax.get_xticks()[::5])
        hep.cms.label("Preliminary", data=True, rlabel=status)
        fig.savefig(output_folder + '/hit-heatmap', dpi=100)
    plt.close()

    return np.max(h.values())


def xy_heatmap(output_folder, status, comment, config: DotMap, max_hits=250,):

    # open root file with data and extract information
    with uproot.open(output_folder + "data.root:events") as file:
        events = file.arrays(
            ["Strip_X", "Strip_Y", "Strip_Strip_num", "Strip_Hit_position"], library="ak")

    # only look at the tail of BX_TDC
    fig, ax = plt.subplots(figsize=(14, 12))

    norm = mcolors.LogNorm(vmin=1, vmax=max_hits)
    cmap = cm.OrRd
    sm = cm.ScalarMappable(cmap=cmap, norm=norm)
    sm.set_array([])
    cbar = fig.colorbar(sm, orientation='vertical', ax=ax)
    cbar.set_label('Hits [1/cm²]')

    for strip in range(48):
        for i in range(-10, 160):
            x_poly, y_poly = get_strip_trapezoid(
                strip=strip,
                hit_position=(i, i + 1),
                cham_type=config.detector.version,
                trig_position=config.detector.position,
            )

            poly = np.swapaxes(np.array([x_poly, y_poly]), 0, 1)
            poly_area = Polygon(poly).area
            coords = np.swapaxes(np.array(
                [ak.flatten(events["Strip_X"]), ak.flatten(events["Strip_Y"])]), 0, 1)

            rate = ak.sum(
                (events["Strip_Strip_num"] == strip) &
                (events["Strip_Hit_position"] > i) &
                (events["Strip_Hit_position"] < i + 1)
            ) / poly_area

            ax.add_patch(patches.Polygon(
                xy=list(zip(x_poly, y_poly)), fill=True, edgecolor=cmap(norm(rate)), facecolor=cmap(norm(rate))))

    # plot the pcb outline
    x_poly, y_poly = get_pcb_outline(cham_type=config.detector.version, trig_position=config.detector.position)

    ax.add_patch(patches.Polygon(
        xy=list(zip(x_poly, y_poly)), fill=False))

    ax.text(0.60, 0.92, comment, fontsize=20, ha='left', va='top', transform=ax.transAxes, bbox=dict(
        facecolor='white', alpha=0.6, edgecolor='black', boxstyle='round,pad=1'))
    hep.cms.label("Preliminary", data=True, rlabel=status)
    ax.set_xlabel("X [cm]")
    ax.set_ylabel("Y [cm]")
    ax.set_xlim(-5, 60)
    ax.set_ylim(-10, 160)
    fig.savefig(output_folder + '/xy-heatmap', dpi=100)
    plt.close()


def hit_heatmap_muon_window(output_folder, status, comment):

    # open root file with data and extract information
    with uproot.open(output_folder + "data.root:events") as file:
        events = file.arrays(
            ["Strip_Strip_num", "Strip_Hit_position", "Strip_HR_value", "L1T"], library="ak")

    # create histogram for muon window
    h = (
        Hist.new.Var(np.linspace(-100, 1600, 300), name="hit_HR_time",
                     label="Hit time from high radius [ns]")
    ).Weight()
    h.fill(hit_HR_time=ak.flatten(events["Strip_HR_value"] - events["L1T"]))

    muon_windowpeak = h.axes[0].edges[np.argmax(h.values())]

    hit_mask = ((events["Strip_HR_value"] - events["L1T"]) > muon_windowpeak -
                25) & ((events["Strip_HR_value"] - events["L1T"]) < muon_windowpeak + 25)

    h = (
        Hist.new.Int(1, 49, name="Strip_num", label="Strip Number")
        .Var(np.linspace(0, 150, 151), name="Hit_position", label="Hit position from low radius [cm]")
    ).Weight()

    h.fill(Strip_num=ak.flatten(events["Strip_Strip_num"][hit_mask]) + 1, Hit_position=ak.flatten(
        events["Strip_Hit_position"][hit_mask]))
    try:
        h.plot2d(ax=ax, norm=LogNorm(), cmap='GnBu')
    except:
        print("\x1b[1;49;31m" + "no hits in the muon window, hit-heatmap-muon-window.png plot will be empty" + "\x1b[0m")
    else:
        fig, ax = plt.subplots(figsize=(12, 12))
        ax.text(0.60, 0.92, comment, fontsize=20, ha='left', va='top', transform=ax.transAxes,
                bbox=dict(facecolor='white', alpha=0.6, edgecolor='black', boxstyle='round,pad=1'))
        ax.set_xticks(ax.get_xticks()[::5])
        hep.cms.label("Preliminary", data=True, rlabel=status)
        fig.savefig(output_folder + '/hit-heatmap-muon-window', dpi=100)
        plt.close()


def xy_heatmap_muon_window(output_folder, status, comment, config: DotMap, max_hits=250):

    # open root file with data and extract information
    with uproot.open(output_folder + "data.root:events") as file:
        events = file.arrays(
            ["Strip_X", "Strip_Y", "Strip_HR_value", "L1T", "Strip_Strip_num", "Strip_Hit_position"], library="ak")

    # create histogram for muon window
    h = (
        Hist.new.Var(np.linspace(-100, 1600, 300), name="hit_HR_time",
                     label="Hit time from high radius [ns]")
    ).Weight()
    h.fill(hit_HR_time=ak.flatten(events["Strip_HR_value"] - events["L1T"]))

    muon_windowpeak = h.axes[0].edges[np.argmax(h.values())]

    hit_mask = ((events["Strip_HR_value"] - events["L1T"]) > muon_windowpeak -
                25) & ((events["Strip_HR_value"] - events["L1T"]) < muon_windowpeak + 25)

    # only look at the tail of BX_TDC
    fig, ax = plt.subplots(figsize=(14, 12))

    norm = mcolors.LogNorm(vmin=1, vmax=max_hits)
    cmap = cm.OrRd
    sm = cm.ScalarMappable(cmap=cmap, norm=norm)
    sm.set_array([])
    cbar = fig.colorbar(sm, orientation='vertical', ax=ax)
    cbar.set_label('Hits [1/cm²]')

    # plot all the bins as polygons
    for strip in range(48):
        for i in range(-10, 160):
            x_poly, y_poly = get_strip_trapezoid(
                strip=strip,
                hit_position=(i, i + 1.),
                cham_type=config.detector.version,
                trig_position=config.detector.position,
            )

            poly = np.swapaxes(np.array([x_poly, y_poly]), 0, 1)
            poly_area = Polygon(poly).area

            rate = ak.sum(
                (events["Strip_Strip_num"][hit_mask] == strip) &
                (events["Strip_Hit_position"][hit_mask] > i) &
                (events["Strip_Hit_position"][hit_mask] < i + 1)
            ) / poly_area

            ax.add_patch(patches.Polygon(
                xy=list(zip(x_poly, y_poly)), fill=True, edgecolor=cmap(norm(rate)), facecolor=cmap(norm(rate))))

    # plot the pcb outline
    x_poly, y_poly = get_pcb_outline(cham_type=config.detector.version, trig_position=config.detector.position)

    ax.add_patch(patches.Polygon(
        xy=list(zip(x_poly, y_poly)), fill=False))

    ax.text(0.60, 0.92, comment, fontsize=20, ha='left', va='top', transform=ax.transAxes, bbox=dict(
        facecolor='white', alpha=0.6, edgecolor='black', boxstyle='round,pad=1'))
    hep.cms.label("Preliminary", data=True, rlabel=status)
    ax.set_xlabel("X [cm]")
    ax.set_ylabel("Y [cm]")
    ax.set_xlim(-5, 60)
    ax.set_ylim(-10, 160)
    fig.savefig(output_folder + '/xy-heatmap-muon', dpi=100)
    plt.close()


def hit_distribution(output_folder, status, comment):

    with uproot.open(output_folder + "data.root:events") as file:
        events = file.arrays(
            ["Strip_Strip_num", "Strip_Hit_position"], library="ak")

    # create histogram and fill
    h = (
        Hist.new.Int(1, 49, name="Strip_num", label="Strip Number")
    ).Weight()
    h.fill(Strip_num=ak.flatten(events["Strip_Strip_num"]) + 1)

    fig, ax = plt.subplots(figsize=(12, 12))

    h.plot(ax=ax, histtype="fill", label="data")
    ax.step(x=np.array([1] + list(h.axes[0].edges)) - 1, y=[0] + list(h.values()) + [0],
            where="post", color="black")

    ax.fill_between(x=(h.axes[0].edges - 1),
                    y1=list(h.values() - np.sqrt(h.values())) +
                    list((h.values() - np.sqrt(h.values()))[-1:]),
                    y2=list(h.values() + np.sqrt(h.values())) +
                    list((h.values() + np.sqrt(h.values()))[-1:]),
                    label='Stat. Unc.', step='post', **hatch_style
                    )

    ax.text(0.60, 0.92, comment, fontsize=20, ha='left', va='top', transform=ax.transAxes, bbox=dict(
        facecolor='white', alpha=0.6, edgecolor='black', boxstyle='round,pad=1'))
    ax.set_xticks(ax.get_xticks()[::5])
    ax.set_ylabel("Hits")
    ax.set_xlim(left=-0.5, right=48.5)
    # ax.legend()
    hep.cms.label("Preliminary", data=True, rlabel=status)
    fig.savefig(output_folder + '/strip-hitcnt', dpi=100)
    plt.close()


def hit_timing_HR(output_folder, status, comment):

    with uproot.open(output_folder + "data.root:events") as file:
        events = file.arrays(
            ["L1T", "Strip_HR_value"], library="ak")

    # create histogram and fill
    h = (
        Hist.new.Var(np.linspace(-100, 1600, 300), name="hit_HR_time",
                     label="Hit time from high radius [ns]")
    ).Weight()
    h.fill(hit_HR_time=ak.flatten(events["Strip_HR_value"] - events["L1T"]))

    fig, ax = plt.subplots(figsize=(12, 12))

    h.plot(ax=ax, histtype="fill", label="data")
    ax.step(x=h.axes[0].edges, y=list(h.values()) +
            [0], where="post", color="black")
    ax.fill_between(x=h.axes[0].edges[:-1], y1=h.values() - np.sqrt(h.values()), y2=h.values() + np.sqrt(h.values()),
                    label='Stat. Unc.', step='post', **hatch_style
                    )

    ax.text(0.60, 0.92, comment, fontsize=20, ha='left', va='top', transform=ax.transAxes, bbox=dict(
        facecolor='white', alpha=0.6, edgecolor='black', boxstyle='round,pad=1'))

    ax.set_ylabel("Hits")
    # ax.legend()
    hep.cms.label("Preliminary", data=True, rlabel=status)
    fig.savefig(output_folder + '/hit_timing_HR', dpi=100)
    plt.close()


def main_plots(
    config: DotMap,
    hv: int,
) -> None:

    comment = "GIF++ test beam June 24\n" + "--------------------------------\n" + "1.4mm double gap iRPC\n" + \
        "FEB v2.3 Petiroc 2C\n" + \
        f"threshold ~ {config.styling.dac_threshold}fC (dac{config.styling.dac})\n" + \
        f"GIF++ source {config.styling.source}"

    # plot for all links
    any_failed_plots = False
    for link_number in range(config.detector.links):
        output_path = f"{config.paths.output}Chamber{config.detector[f'chamber{link_number + 1}']}/HV{hv}/"

        if converted(config, output_path + "data.root"):
            max_hit = hit_heatmap(output_path, config.styling.cmslabel, comment)
            hit_heatmap_muon_window(output_path, config.styling.cmslabel, comment)
            xy_heatmap(output_path, config.styling.cmslabel, comment, config, max_hit)
            xy_heatmap_muon_window(output_path, config.styling.cmslabel, comment, config, max_hit)

            hit_timing_HR(output_path, config.styling.cmslabel, comment)
            hit_distribution(output_path, config.styling.cmslabel, comment)
        else:
            with uproot.open(output_path + "data.root:events") as file:
                events = file.arrays(
                    ["Strip_Strip_num"], library="ak")
            print("\x1b[1;49;31m" + "WARNING: data not correctly converted!" + "\x1b[0m")
            print(
                "\x1b[1;49;31m" + f"might be that number of triggers ({config.scan.triggers}) does not match data length ({len(events)})" + "\x1b[0m")

            any_failed_plots = True

    return any_failed_plots


def fast_plots(
    config: DotMap,
    hv: int,
) -> None:

    comment = "GIF++ test beam June 24\n" + "--------------------------------\n" + \
        "1.4mm double gap iRPC\n" + \
        "FEB v2.3 Petiroc 2C\n" + \
        f"threshold ~ {config.styling.dac_threshold}fC (dac{config.styling.dac})\n" + \
        f"GIF++ source {config.styling.source}"

    # plot for all links
    any_failed_plots = False
    for link_number in range(config.detector.links):
        output_path = f"{config.paths.output}Chamber{config.detector[f'chamber{link_number + 1}']}/HV{hv}/"

        if converted(config, output_path + "data.root"):
            # hit_heatmap(output_path, config.styling.cmslabel, comment)
            # hit_heatmap_muon_window(output_path, config.styling.cmslabel, comment)

            # hit_timing_HR(output_path, config.styling.cmslabel, comment)
            hit_distribution(output_path, config.styling.cmslabel, comment)
        else:
            with uproot.open(output_path + "data.root:events") as file:
                events = file.arrays(
                    ["Strip_Strip_num"], library="ak")
            print("\x1b[1;49;31m" + "WARNING: data not correctly converted!" + "\x1b[0m")
            print(
                "\x1b[1;49;31m" + f"might be that number of triggers ({config.scan.triggers}) does not match data length ({len(events)})" + "\x1b[0m")

            any_failed_plots = True
    return any_failed_plots
