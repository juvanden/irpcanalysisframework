# version 6

numpy
pyyaml==5.1
awkward==2.4.6
uproot==5.1.2
pyarrow==13.0.0
matplotlib==3.8.3
mplhep==0.3.46
hist==2.7.2
scipy==1.12.0
pandas==2.2.1
openpyxl==3.1.2
DotMap
flatten-dict
shapely